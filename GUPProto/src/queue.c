#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"

#define     q_aborted(qh)   (((qh)->q_flags & 0x80000000) == 0x80000000)
#define     q_stopped(qh)   (((qh)->q_flags & 0x40000000) == 0x40000000)

#define     q_enq_active(qh)    (!q_aborted(qh) && !q_stopped(qh))
#define     q_deq_active(qh)    (!q_aborted(qh))

#define     q_inactive_status(qh)   (q_aborted(qh) ? Q_ABORTED : (q_stopped(qh) ? Q_STOPPED : Q_FAIL))

// Create a new q element that holds the object of specified size
void* q_new(void* object, int size)
{
    void*   newmem = NULL;
    void*   data = NULL;

    if((newmem = malloc(size + sizeof(Q_ELEM))) == NULL)
    {
        return NULL;
    }

    // Get pointer to data
    data = q_data(newmem);

    // copy the data into new element
    memcpy(data, object, size);

    return newmem;
}

// Add a new queue element to the head of queue
int q_ahead(Q_HEAD* q_head, Q_ELEM* q_elem)
{
    q_elem->q_forw = q_head->q.q_forw;
    q_elem->q_back = NULL;
    q_head->q.q_forw = q_elem;

    if(q_elem->q_forw)
    {
        q_elem->q_forw->q_back = q_elem;
    }
    else
    {
        q_head->q.q_back = q_elem;
    }

    q_head->q_elems++;

    return Q_OK;
}

int q_atail(Q_HEAD* q_head, Q_ELEM* q_elem)
{
    q_elem->q_back = q_head->q.q_back;
    q_elem->q_forw = NULL;
    q_head->q.q_back = q_elem;

    if(q_elem->q_back)
    {
        q_elem->q_back->q_forw = q_elem;
    }
    else
    {
        q_head->q.q_forw = q_elem;
    }

    q_head->q_elems++;

    return Q_OK;
}

static void q_unlink(Q_HEAD* q_head, Q_ELEM* q_elem)
{
	Q_ELEM		*this_elem = q_elem;
	Q_ELEM		*prev_elem;
	Q_ELEM		*next_elem;
	
    /* --- Assume q_elem is there in the queue --- */
    /* --- Update the forward pointers ---- */
    if( (prev_elem = this_elem->q_back) != NULL )
    {
        prev_elem->q_forw = this_elem->q_forw;
    }
    else
    {
        q_head->q.q_forw = this_elem->q_forw;
    }

    /* --- Update the backwards pointers --- */
    if( (next_elem = this_elem->q_forw) != NULL )
    {
        next_elem->q_back = this_elem->q_back;
    }
    else
    {
        q_head->q.q_back = this_elem->q_back;
    }
}

void* q_rhead(Q_HEAD* q_head)
{
    Q_ELEM* this_elem = q_first(q_head);

    if(this_elem != NULL)
    {
        q_unlink(q_head, this_elem);
        q_head->q_elems--;

        return (void*)this_elem;
    }

    return NULL;
}

void* q_rtail(Q_HEAD* q_head)
{
    Q_ELEM* this_elem = q_last(q_head);

    if(this_elem != NULL)
    {
        q_unlink(q_head, this_elem);
        q_head->q_elems--;

        return (void*)this_elem;
    }

    return NULL;
}

void* q_remove(Q_HEAD* q_head, Q_ELEM* q_elem)
{
	Q_ELEM		*this_elem;
	Q_ELEM		*prev_elem;
	Q_ELEM		*next_elem;
	
	/* --- Look for the element in the list ---- */
	for(this_elem = q_head->q.q_forw;
		this_elem;
		this_elem = this_elem->q_forw)
	{
		/* --- Is this the element we are looking for --- */
		if( this_elem == q_elem )
		{
            q_unlink(q_head, this_elem);

			/* --- Return a pointer to the element --- */
			return( (void *)this_elem );
		}
	}

	/* --- Did not find the element on the queue --- */
	return NULL;
}

void* q_findfirst(Q_HEAD* q_head, Q_D_COMP_FN q_d_comp, void* param)
{
	/* --- Look for the first element in the list --- */
	return(q_findnext(q_first(q_head), q_d_comp, param ));
}

void* q_findnext(Q_ELEM* q_start, Q_D_COMP_FN q_d_comp, void* param)
{
	Q_ELEM		*q_elem;
	
	/* --- Look for the element in the list --- */
	for(q_elem = q_start;
		q_elem;
		q_elem = q_next(q_elem) )
	{
		/* --- Is this the element we are seeking --- */
		if( (*q_d_comp)(q_data(q_elem), param))
		{
			/* --- Return a pointer to the element --- */
			return((void *)q_elem);
		}
	}

	/* --- Did not find the element on the queue --- */
	return NULL;
}

void q_init(Q_HEAD* q_head, int q_flags, int q_size)
{
    // Initialize the Q_HEAD
    q_head->q.q_back = NULL;
    q_head->q.q_forw = NULL;

    q_head->q_elems = 0;
    q_head->q_flags = q_flags;

    if(q_head->q_elems < 0)
        q_size = 0x7fffffff;

    q_head->q_size = q_size;

    // initialize the mutex and condition variables
    pthread_mutex_init(&q_head->q_mutex, NULL);
    pthread_cond_init(&q_head->q_dq_cond, NULL);
    pthread_cond_init(&q_head->q_eq_cond, NULL);
}

void q_abort(Q_HEAD* q_head)
{
    Q_ELEM* object = NULL;

    q_lock(q_head);

    // Set the top bit of q_flag
    q_head->q_flags |= 0x80000000;

    // Go through the whole list and free objects.
    while(q_head->q.q_forw)
    {
        // remove object from queue. 
        object = q_remove(q_head, q_head->q.q_forw);

        // Free the object. 
        free(object);
    }

    q_head->q_elems = 0;

    // Signal dequeue and enqueue
    q_sync_print(q_head, stderr, "%s: SIGNAL NON EMPTY DESTROY COND\n", __FUNCTION__);
    q_signal_non_empty(q_head);
    q_sync_print(q_head, stderr, "%s: SIGNAL NON FULL DESTROY COND\n", __FUNCTION__);
    q_signal_non_full(q_head);

    q_unlock(q_head);
}

void q_stop(Q_HEAD* q_head)
{
    q_lock(q_head);

    // Set the STOP bit
    q_head->q_flags |= 0x40000000;

    // Let us signal enqueue and dequeue
    q_sync_print(q_head, stderr, "%s: SIGNAL NON EMPTY DESTROY COND\n", __FUNCTION__);
    q_signal_non_empty(q_head);
    q_sync_print(q_head, stderr, "%s: SIGNAL NON FULL DESTROY COND\n", __FUNCTION__);
    q_signal_non_full(q_head);

    q_unlock(q_head);
}

int q_enqueue(Q_HEAD* q_head, void* data, int size)
{
    Q_ELEM* q_elem = NULL;

    if(!q_enq_active(q_head))
    {
        return q_inactive_status(q_head);
    }

    if(!q_synchronized(q_head) && q_full(q_head))
    {
        q_debug_print(stderr, "%s: OVERFLOW. Q FULL\n", __FUNCTION__);
        return Q_OVERFLOW;
    }

    /* ---- Proceed to add (with sync if required) ----- */
    q_sync_print(q_head, stderr, "%s: LOCK MUTEX\n", __FUNCTION__);
    q_lock(q_head);

    while(q_full(q_head))
    {
        if(!q_enq_active(q_head))
        {
            q_sync_print(q_head, stderr, "%s: Q ABORTED-STOPPED. UNLOCK MUTEX\n", __FUNCTION__);
            q_unlock(q_head);
            return q_inactive_status(q_head);
        }

        // Waiting on full condition
        q_wait_for_non_full(q_head);
    }

    q_elem = q_new(data, size);
    
    if(q_elem == NULL)
    {
        // Failed to create a q element
        q_sync_print(q_head, stderr, "%s: ELEM CREATE FAIL. UNLOCK MUTEX\n", __FUNCTION__);
        q_unlock(q_head);
        return Q_FAIL;
    }

    // Add this to the end
    q_atail(q_head, q_elem);

    q_sync_print(q_head, stderr, "%s: SIGNAL NON EMPTY CONDITION\n", __FUNCTION__);
    q_signal_non_empty(q_head);
    q_sync_print(q_head, stderr, "%s: UNLOCK MUTEX\n", __FUNCTION__);
    q_unlock(q_head);

    return Q_OK;
}

int q_dequeue(Q_HEAD* q_head, void* data, int size)
       
{
    Q_ELEM* this_elem = NULL;
    int     is_q_stopped = 0;
    int     retval       = Q_OK;

    if(q_aborted(q_head))
    {
        return Q_ABORTED;
    }

    if(!q_synchronized(q_head) && q_empty(q_head))
    {
        q_debug_print(stderr, "%s: Q EMPTY. UNDERFLOW\n", __FUNCTION__);
        return Q_UNDERFLOW;
    }

    q_sync_print(q_head, stderr, "%s: LOCK MUTEX\n", __FUNCTION__);
    q_lock(q_head);

    while(q_empty(q_head))
    {
        // Check before we wait. This is to take care of logical error
        // where the program might signal before a wait
        if(q_aborted(q_head))
        {
            q_sync_print(q_head, stderr, "%s: Q ABORTED. UNLOCK MUTEX\n", __FUNCTION__);
            q_unlock(q_head);
            return Q_ABORTED;
        }

        if(q_stopped(q_head))
        {
            q_sync_print(q_head, stderr, "%s: Q STOPPED. UNLOCK MUTEX\n", __FUNCTION__);
            q_unlock(q_head);
            return Q_STOPPED;
        }

        // Wait for q to be filled up
        q_sync_print(q_head, stderr, "%s: WAITING ON CONDITION\n", __FUNCTION__);
        q_wait_for_non_empty(q_head);
    }

    this_elem = q_rhead(q_head);

    /* --- copy the data into the provided ptr --- */
    memcpy(data, q_data(this_elem), size);

    /* --- free this_elem after dequeue --- */
    free(this_elem);

    if(q_stopped(q_head) && q_empty(q_head))
    {
        retval = Q_STOPPED;
    }

    q_sync_print(q_head, stderr, "%s: SIGNAL NON FULL CONDITION\n", __FUNCTION__);
    q_signal_non_full(q_head);
    q_sync_print(q_head, stderr, "%s: UNLOCK MUTEX\n", __FUNCTION__);
    q_unlock(q_head);

    return retval;
}

int q_freeall(Q_HEAD* q_head)
{
    void        *object;

    q_sync_print(q_head, stderr, "%s: LOCK MUTEX\n", __FUNCTION__);
    q_lock(q_head);

    // Go through the whole list and free objects.
    while(q_head->q.q_forw)
    {
        // remove object from queue. 
        object = q_remove(q_head, q_head->q.q_forw);

        // Free the object. 
        free(object);
    }

    q_head->q_elems = 0;

    q_sync_print(q_head, stderr, "%s: UNLOCK MUTEX\n", __FUNCTION__);
    q_unlock(q_head);

    // Return an Ok status.
    return Q_OK;
}
