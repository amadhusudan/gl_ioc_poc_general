/*!--------------------------------------------------------------------------

    @file     queue.h
    @date     17/07/2013

    Generic synchronized queueing package.
*/
/*

(C) Copyright 2013 - Madhusudan
---------------------------------------------------------------------------*/

#ifndef _QUEUE_H
#define _QUEUE_H

#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Queue Element.
 * Queue Element Structure.
 */
typedef struct q_elem
{
	struct q_elem*  q_forw;		//<! The next element in the queue
	struct q_elem*  q_back;		//<! The previous element in the queue
}Q_ELEM;

// ! Data copy func pointer. defaults to memcpy
typedef void*   (*Q_D_COPY_FN)  (void*, const void*, size_t);

// ! Data free func pointer. defaults to free
typedef void    (*Q_D_FREE_FN)  (void*);

// ! Comparison func pointer
typedef int     (*Q_D_COMP_FN)  (void*, void*);

/**
 * default flags for initializing a Q
 */
#define     Q_FLAG_DEFAULT          0x00000000

/**
 * Synchronized flag for Q.
 * Access to elements is synchronized. Dequeue waits on enqueue
 */
#define     Q_FLAG_SYNCHRONIZED     0x00000001


/**
 * Queue Head
 * Queue Head element. Consists of Queue Elements.
 */
typedef struct q_head
{
	Q_ELEM      q;          //<! The queue element          
	int         q_elems;    //<! The number of elements in Queue
    int         q_flags;    //<! The flags defining the queue
    int         q_size;     //<! Max elems

    pthread_mutex_t q_mutex;    //<! Lock mutex for queue
    pthread_cond_t  q_dq_cond;  //<! condition var for dequeue
    pthread_cond_t  q_eq_cond;  //<! enqueue cond var
}Q_HEAD;

//#define Q_DEBUG

#ifdef Q_DEBUG
#define     q_debug_print(fp, format, args)     fprintf(fp, format, args)
#else
#define     q_debug_print(fp, format, args)
#endif

#define     q_sync_print(qh, fp, format, args) \
    if(q_synchronized((qh)))                   \
        q_debug_print(fp, format, args)

#define     q_synchronized(qh)  ((Q_FLAG_SYNCHRONIZED) == ((qh)->q_flags & Q_FLAG_SYNCHRONIZED))

#define     q_full(qh)  ((qh)->q_size <= (qh)->q_elems)
#define     q_empty(qh) ((qh)->q_elems <= 0)

#define	    q_first(qh)	((qh)->q.q_forw)
#define	    q_last(qh)	((qh)->q.q_back)
#define 	q_next(q)	((q)->q_forw)
#define	    q_prev(q)	((q)->q_back)
#define	    q_data(q)	((void *)((char *)(q) + sizeof(Q_ELEM)))
#define	    q_elem(d)	((void *)((char *)(d) - sizeof(Q_ELEM)))
#define     q_empty(qh) ((qh)->q_elems <= 0)

#define     q_lock(qh)                          \
    if(q_synchronized((qh)))                    \
        pthread_mutex_lock(&(qh)->q_mutex)

#define     q_unlock(qh)                        \
    if(q_synchronized((qh)))                    \
        pthread_mutex_unlock(&(qh)->q_mutex)

#define     q_wait_for_non_full(qh)                              \
    if(q_synchronized((qh)))                                \
        pthread_cond_wait(&(qh)->q_eq_cond, &(qh)->q_mutex)

#define     q_wait_for_non_empty(qh)                             \
    if(q_synchronized((qh)))                                \
        pthread_cond_wait(&(qh)->q_dq_cond, &(qh)->q_mutex)

#define     q_signal_non_full(qh)               \
    if(q_synchronized((qh)))                    \
    pthread_cond_signal(&(qh)->q_eq_cond)

#define     q_signal_non_empty(qh)              \
    if(q_synchronized((qh)))                    \
        pthread_cond_signal(&(qh)->q_dq_cond)

#define     Q_OK            0
#define     Q_FAIL          -1
#define     Q_OVERFLOW      -2
#define     Q_UNDERFLOW     -3
#define     Q_ABORTED       -4
#define     Q_STOPPED       -5

/**
 * Queue Append at Head
 * Add a Q_ELEM to the Q_HEAD to the front of the Queue
 *
 * @param[in]  q_head  q_head is the head of the queue
 * @param[in]  q_elem  q_elem is the element to add to the queue
 *
 * @return     Q_OK    successfully added element to queue
 */
extern  int     q_ahead(Q_HEAD* q_head, Q_ELEM* q_elem);

/**
 * Queue Append at tail
 * Add a Q_ELEM to the Q_HEAD to the end of the Queue
 *
 * @param[in]  q_head  q_head is the head of the queue
 * @param[in]  q_elem  q_elem is the element to add to the queue
 *
 * @return     Q_OK    successfully added element to queue
 */
extern  int     q_atail(Q_HEAD* q_head, Q_ELEM* q_elem);


/**
 * Remove item from head
 * Remove an element from the front Queue and return its pointer
 * This does not delete the element
 *
 * @param[in]  q_head   head of the queue
 *
 * @return     Q_ELEM*  The pointer to the Q_ELEM (returned as void*)
               NULL     No Items in Q
 */
extern void*    q_rhead(Q_HEAD* q_head);

/**
 * Remove item from tail
 * Remove an element from the back Queue and return its pointer
 * This does not delete the element
 *
 * @param[in]  q_head   head of the queue
 *
 * @return     Q_ELEM*  The pointer to the Q_ELEM (returned as void*)
               NULL     No Items in Q
 */
extern void*    q_rtail(Q_HEAD* q_head);

/**
 * Remove item from Queue
 * Remove an element from the Queue and return its pointer
 * This does not delete the element
 *
 * @param[in]  q_head   head of the queue
 * @param[in]  q_elem   element to remove from the queue
 *
 * @return     Q_ELEM*  The pointer to the Q_ELEM (returned as void*)
               NULL     element not found. NULL returned
 */
extern  void*   q_remove(Q_HEAD* q_head, Q_ELEM* q_elem);

/**
 * Create a new Q_ELEM
 *
 * @param[in]  data     the data to store in the queue
 * @param[in]  size     size of the data
 *
 * @return     Q_ELEM*  the new queue element with data copied
 */
extern  void*   q_new(void* data, int size);

/**
 * Initialize a queue
 * Initialize and enable a queue
 * 
 * @param[in]  q_head     the queue to be initialized
 * @param[in]  q_flags    the flags for the queue
 * @param[in]  q_size     max elems in queue. -1 indicated INT_MAX
 */
extern  void    q_init(Q_HEAD* q_head, int q_flags, int q_size);

/**
 * Stop a queue
 * Stop a queue. If synchronized send broadcast signal to all threads
 * 
 * @param[in]  q_head     the queue to be initialized
 */
extern  void    q_abort(Q_HEAD* q_head);

/**
 * Add to the Queue
 * Add element to queue at the end. Classic Queue ENQUEUE
 *
 * @param[in]  q_head   the queue head of the queue 
 * @param[in]  data     the data to store in the queue
 * @param[in]  size     size of the data
 * @return     Q_OK     SUCCESS
 *             Q_FAIL   Failed to add the element
 */
extern  int     q_enqueue(Q_HEAD* q_head, void* data, int size);

/**
 * Get element from Queue
 * Get element at teh front of the queue. Classic DEQUEUE
 *
 * @param[in]  q_head   the queue head of the queue 
 * @param[out] data     the data to store in the queue
 * @param[in]  size     size of the data
 * @return     Q_OK     SUCCESS
 *             Q_FAIL   Failed to dequeue.
 */
extern  int     q_dequeue(Q_HEAD* q_head, void* data, int size);

/**
 * Empty the Queue
 * Empty the Queue. Remove all elements.
 *
 * @param[in]  q_head   head of the queue
 *
 * @return     Q_OK     all elements removed from queue
 */
extern  int     q_freeall(Q_HEAD* q_head);

/**
 * Find first occurence of element in Queue
 * Find the first occurence of an element int the queue
 *
 * @param[in]  q_head     head of the queue
 * @param[in]  q_d_comp   the comparison function
 * @param[in]  param      the parameter to search for
 *
 * @return     Q_ELEM*  The pointer to the Q_ELEM (returned as void*)
               NULL     element not found. NULL returned
 */
extern  void*   q_findfirst(Q_HEAD* q_head, Q_D_COMP_FN q_d_comp, void* param);

/**
 * Find first occurence of element in Queue
 * Find the first occurence of an element int the queue
 *
 * @param[in]  q_elem     element in queue from where to start searching
 * @param[in]  q_d_comp   the comparison function
 * @param[in]  param      the parameter to search for
 *
 * @return     Q_ELEM*  The pointer to the Q_ELEM (returned as void*)
               NULL     element not found. NULL returned
 */
extern  void*   q_findnext(Q_ELEM* q_head, Q_D_COMP_FN q_d_comp, void* param);

#ifdef __cplusplus
}
#endif

#endif
