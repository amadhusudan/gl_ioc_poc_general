#include <SDL2/sdl.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <pthread.h>
#include "queue.h"
#include "media.h"

//#include <conio.h>

int framenumber = 0;
int delay = 1000;


Uint32 fps_regulator(Uint32 interval, void* f)
{
    printf("avg_fps = %d\n", framenumber);
    framenumber = 0;
    return interval;
}

/* static Uint32 refresh_timer(Uint32 interval, void* data) */
/* { */
/*     SDL_Event e; */
/*     e.type = GUP_VIDEO_REFRESH_EVENT; */
/*     e.user.data1 = data; */
/*     SDL_PushEvent(&e); */
/*     return 0;   /\* means stop timer *\/ */
/* } */

/* static void schedule_refresh(MEDIA_PLAYBACK_ITEM* media_pb_item, int delay) */
/* { */
/*     SDL_AddTimer(delay, refresh_timer, media_pb_item); */
/* } */

/* static void video_refresh(void* userdata, SDL_Renderer* renderer) */
/* { */
/*     MEDIA_PLAYBACK_ITEM* media_pb_item = (MEDIA_PLAYBACK_ITEM*)userdata; */
/*     MEDIA_RENDER_IMAGE_Q* v_image_q = &media_pb_item->video_images_q; */

/*     if(media_pb_item->video_stream_idx != INVALID_MEDIA_STREAM_IDX) */
/*     { */
/*         if(v_image_q->size == 0) */
/*         { */
/*             schedule_refresh(media_pb_item, 1); */
/*         } */
/*         else */
/*         { */
/*             MEDIA_VIDEO_TEXTURE* mvt = &v_image_q->video_textures[v_image_q->read_idx]; */
            
/*             schedule_refresh(media_pb_item, 50); */

/*             SDL_RenderClear(renderer); */
/*             SDL_RenderCopy(renderer, mvt->texture, NULL, NULL); */
/*             SDL_RenderPresent(renderer); */

/*             //video_display(mvt); */
/*             if(++v_image_q->read_idx == MEDIA_VIDEO_DISPLAY_FRAME_Q_SIZE) */
/*             { */
/*                 v_image_q->read_idx = 0; */
/*             } */

/*             pthread_mutex_lock(&v_image_q->q_mutex); */
/*             v_image_q->size--; */
/*             pthread_cond_signal(&v_image_q->q_cond); */
/*             pthread_mutex_unlock(&v_image_q->q_mutex); */
            
/*             framenumber++; */
/*         } */
/*     } */
/*     else */
/*     { */
/*         schedule_refresh(media_pb_item, 100); */
/*     } */
/* } */

/* void alloc_video_texture(void* user_data, SDL_Renderer* renderer) */
/* { */
/*     MEDIA_PLAYBACK_ITEM* media_pb_item = (MEDIA_PLAYBACK_ITEM*)user_data; */
/*     MEDIA_RENDER_IMAGE_Q* v_image_q = &media_pb_item->video_images_q; */
/*     MEDIA_VIDEO_TEXTURE* mvt = &v_image_q->video_textures[v_image_q->write_idx]; */

/*     // Create the texture object for the media file */
/*     if(mvt->texture) */
/*     { */
/*         /\* fprintf(stderr, "Deleting texture\n"); *\/ */
/*         SDL_DestroyTexture(mvt->texture); */
/*     } */

/*     /\* fprintf(stderr, "Alloc Texture\n"); *\/ */
/*     mvt->texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_YV12, */
/*                                      SDL_TEXTUREACCESS_STREAMING, */
/*                                      media_pb_item->v_codec_ctx->width, */
/*                                      media_pb_item->v_codec_ctx->height); */

/*     mvt->width = media_pb_item->v_codec_ctx->width; */
/*     mvt->height = media_pb_item->v_codec_ctx->height; */
    
/*     pthread_mutex_lock(&v_image_q->q_mutex); */
/*     mvt->allocated = 1; */
/*     pthread_cond_signal(&v_image_q->q_cond); */
/*     pthread_mutex_unlock(&v_image_q->q_mutex); */
/* } */

static void video_refresh(MEDIA_PLAYBACK_ITEM*  media_pb_item, 
                          const AVFrame*        frame,
                          SDL_Renderer*         renderer,
                          SDL_Texture*          texture)
{
    int size = media_pb_item->v_codec_ctx->width * media_pb_item->v_codec_ctx->height;
    uint8_t* pixels;
    int      pitch;

    SDL_LockTexture(texture, NULL, (void **)&pixels, &pitch);
    memcpy(pixels, frame->data[0], size);
    memcpy(pixels + size, frame->data[2], size/4);
    memcpy(pixels + size*5/4, frame->data[1], size/4);
    SDL_UnlockTexture(texture);
    SDL_UpdateTexture(texture, NULL, pixels, pitch);

    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
}

#define AVCODEC_MAX_AUDIO_FRAME_SIZE 192000

static void audio_callback(void* userdata, Uint8* stream, int len)
{
    MEDIA_PLAYBACK_ITEM* media_pb_item = (MEDIA_PLAYBACK_ITEM*)userdata;
    static uint8_t audio_buf[(AVCODEC_MAX_AUDIO_FRAME_SIZE * 3) / 2];
    static unsigned int audio_buf_size  = 0;
    static unsigned int audio_buf_index = 0;

    uint8_t** audio_dst_data = NULL;
    int       audio_dst_linesize = 0;
    int       audio_dst_bufsize = 0;
    int       previous_samples = 0;
    int       len1 = 0;
    
    while(len > 0)
    {
        if(audio_buf_index >= audio_buf_size)
        {
            /* Get the next audio data */
            audio_dst_data = decode_audio(media_pb_item, audio_buf, &audio_dst_bufsize);
            if(audio_dst_bufsize <= 0)
            {
                audio_buf_size = 1024;
                memset(audio_buf, 0, audio_buf_size);
            }
            else
            {
                audio_buf_size = audio_dst_bufsize;
            }

            audio_buf_index = 0;
        }

        len1 = audio_buf_size - audio_buf_index;
        if(len1 > len)
        {
            len1 = len;
        }
        memcpy(stream, (uint8_t *)audio_buf + audio_buf_index, len1);
        len -= len1;
        stream += len1;
        audio_buf_index += len1;
    }
}

int playback(const char* filename)
{
    int filenumber = 1;
    const int fps = 24;
    
    //Video
    SDL_Window*     window      = NULL;
    SDL_Renderer*   renderer    = NULL;
    SDL_Texture*    texture     = NULL;

    // Audio    
    SDL_AudioSpec   wanted_spec;
    SDL_AudioSpec   spec;

    // Codec & Threads
    MEDIA_PLAYBACK_ITEM media_pb_item;
    pthread_t           packet_read_thread;
    pthread_t           video_decode_thread;
    pthread_attr_t      thread_attr;
    struct sched_param  sched_param;

    /* Register all codecs */
    av_register_all();

    /* Initialize Codecs and queues and open for playback */
    media_playback_item_init(&media_pb_item, 48);
    if(media_open_for_playback(&media_pb_item, filename) != 0)
    {
        fprintf(stderr, "Unable to initialize and open media for playback\n");
        return -1;
    }

    if(pthread_attr_init(&thread_attr) != 0)
    {
        fprintf(stderr, "Unable to create thread attributes\n");
        return -1;
    }

    pthread_attr_setscope(&thread_attr, PTHREAD_SCOPE_SYSTEM);
    pthread_attr_getschedparam(&thread_attr, &sched_param);
    sched_param.sched_priority = 99;

    // Let us create a decode and enquue thread before we proceed further
    if(pthread_create(&packet_read_thread, &thread_attr, media_read_thread, (void*)&media_pb_item) != 0)
    {
        fprintf(stderr, "Error Creating Packet Read  Thread\n");
        return -1;
    }
    
    // Initialize SDL
    if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER) != 0)
    {
        fprintf(stderr, "Unable to initialize SDL: %s\n", SDL_GetError());
        return -1;
    }
    
    // SDL Audio Stuff
    wanted_spec.freq = media_pb_item.a_codec_ctx->sample_rate;
    wanted_spec.format = AUDIO_S16SYS;
    wanted_spec.channels = media_pb_item.a_codec_ctx->channels;
    wanted_spec.silence = 0;
    wanted_spec.samples = 4096;//SDL_AUDIO_BUFFER_SIZE;
    wanted_spec.callback = audio_callback;
    wanted_spec.userdata = &media_pb_item;

    if(SDL_OpenAudio(&wanted_spec, &spec) < 0)
    {
        fprintf(stderr, "Unable to open Audio: %s\n", SDL_GetError());
        return -1;
    }

    // SDL Video Stuff
    window = SDL_CreateWindow("SDL_RenderClear",
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              media_pb_item.v_codec_ctx->width, media_pb_item.v_codec_ctx->height,
#ifndef _VMACHINE_
                              SDL_WINDOW_OPENGL |
#endif
                              SDL_WINDOW_MAXIMIZED | SDL_WINDOW_SHOWN |SDL_WINDOW_RESIZABLE);
    
    if(window == NULL)
    {
        fprintf(stderr, "Unable to create SDL Window: %s\n", SDL_GetError());
        return -1;
    }
    
    // We must call SDL_CreateRenderer in order for draw calls to affect this window.
    renderer = SDL_CreateRenderer(window, -1, 
#ifndef _VMACHINE_
                                  SDL_RENDERER_ACCELERATED
#else
                                  SDL_RENDERER_SOFTWARE
#endif
        );

    if(renderer == NULL)
    {
        fprintf(stderr, "Unable to create renderer for window: %s\n", SDL_GetError());
        return -1;
    }

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_YV12,
                                SDL_TEXTUREACCESS_STREAMING,
                                media_pb_item.v_codec_ctx->width,
                                media_pb_item.v_codec_ctx->height);
    if(texture == NULL)
    {
        fprintf(stderr, "Unable to create texture for renderer: %s\n", SDL_GetError());
        return -1;
    }

    SDL_PauseAudio(0);

    /* // Let us create a decode and enquue thread before we proceed further */
    /* if(pthread_create(&video_decode_thread, &thread_attr, video_thread, (void*)&media_pb_item) != 0) */
    /* { */
    /*     fprintf(stderr, "Error Creating Packet Decode Enqueue\n"); */
    /*     return -1; */
    /* } */

    SDL_TimerID fps_timer_id = SDL_AddTimer(delay, fps_regulator, NULL);
    /* schedule_refresh(&media_pb_item, 1); */

    while(1)
    {
        SDL_Event e;
        Uint32 timer_start = SDL_GetTicks();

        if(media_pb_item.quit)
        {
            break;
        }

        if(SDL_PollEvent(&e))
        {
            switch(e.type)
            {
            case SDL_QUIT:
                media_pb_item.quit = 1;
                q_abort(&media_pb_item.v_packet_q);
                q_abort(&media_pb_item.a_packet_q);
                break;

            /* case GUP_VIDEO_IMAGE_ALLOC_EVENT: */
            /*     alloc_video_texture(e.user.data1, renderer); */
            /*     break; */

            /* case GUP_VIDEO_REFRESH_EVENT: */
            /*     video_refresh(e.user.data1, renderer); */
            /*     break; */
            }
        }

        const AVFrame* frame = decode_video(&media_pb_item);
        if(frame == NULL)
        {
            break;
        }

        video_refresh(&media_pb_item, frame, renderer, texture);
        framenumber++;
        if ((1000 / fps) > SDL_GetTicks() - timer_start)
        {
            SDL_Delay((1000 / fps) - (SDL_GetTicks() - timer_start));
        }
    }

    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    //Quit SDL
    SDL_Quit();
    
    return 0;
}
