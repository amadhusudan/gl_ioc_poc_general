//
//  media.h
//  
//
//  Created by A Madhusudan on 23/07/13.
//
//

#ifndef _MEDIA_H
#define _MEDIA_H

#include <SDL2/sdl.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include "queue.h"

#ifdef __cplusplus
extern "C" {
#endif

#define     INVALID_MEDIA_STREAM_IDX            -1
#define     MEDIA_VIDEO_FRAME_Q_SIZE            48
#define     MEDIA_VIDEO_DISPLAY_FRAME_Q_SIZE    24
#define     IMAGE_DATA_MAX_PLANES               4

#define     GUP_QUIT_EVENT                  SDL_USEREVENT

/*
#define     GUP_VIDEO_IMAGE_ALLOC_EVENT     GUP_QUIT_EVENT + 1
#define     GUP_VIDEO_REFRESH_EVENT         GUP_VIDEO_IMAGE_ALLOC_EVENT + 1

typedef struct _media_video_texture
{
    SDL_Texture*    texture;
    unsigned int    width;
    unsigned int    height;
    unsigned int    allocated;
}MEDIA_VIDEO_TEXTURE;

typedef struct  _media_render_image_q
{
    MEDIA_VIDEO_TEXTURE video_textures[MEDIA_VIDEO_DISPLAY_FRAME_Q_SIZE];
    unsigned int        read_idx;
    unsigned int        write_idx;
    unsigned int        size;

    pthread_mutex_t     q_mutex;
    pthread_cond_t      q_cond;
}MEDIA_RENDER_IMAGE_Q;
*/

typedef struct _media_playback_item
{
    /* Codec Related Stuff */
    AVFormatContext*    format_ctx;
    AVCodec*            v_codec;
    AVCodecContext*     v_codec_ctx;
    AVCodec*            a_codec;
    AVCodecContext*     a_codec_ctx;
    unsigned int        video_stream_idx;
    unsigned int        audio_stream_idx;

    /* Media Frames Q */
    Q_HEAD              v_packet_q;
    Q_HEAD              a_packet_q;

    /* MEDIA_RENDER_IMAGE_Q    video_images_q; */

    int quit;
    int initialized;
}MEDIA_PLAYBACK_ITEM;

extern void media_playback_item_init(MEDIA_PLAYBACK_ITEM* media_pb_item,
                                     int max_items);

extern int  media_open_for_playback(MEDIA_PLAYBACK_ITEM* media_pb_item,
                                    const char* fqpn);

/* extern void* video_thread(void* data); */
/* extern void* media_read_thread(void* data); */

/* extern int video_thread(void* data); */

extern void* media_read_thread(void* data);

extern const AVFrame*   decode_video(MEDIA_PLAYBACK_ITEM* media_pb_item);
extern uint8_t**        decode_audio(MEDIA_PLAYBACK_ITEM* media_pb_item,
                                     uint8_t*             audio_buffer,
                                     int*                 audio_dst_bufsize);

#ifdef __cplusplus
}
#endif


#endif
