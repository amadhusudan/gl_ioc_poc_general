#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include "queue.h"
#include "media.h"

void media_playback_item_init(MEDIA_PLAYBACK_ITEM* media_pb_item, int max_items)
{
    media_pb_item->format_ctx = NULL;
    media_pb_item->v_codec = NULL;
    media_pb_item->v_codec_ctx = NULL;
    media_pb_item->video_stream_idx = INVALID_MEDIA_STREAM_IDX;
    media_pb_item->audio_stream_idx = INVALID_MEDIA_STREAM_IDX;

    q_init(&media_pb_item->v_packet_q, Q_FLAG_SYNCHRONIZED, MEDIA_VIDEO_FRAME_Q_SIZE);
    q_init(&media_pb_item->a_packet_q, Q_FLAG_SYNCHRONIZED, MEDIA_VIDEO_FRAME_Q_SIZE);

/*
    media_pb_item->video_images_q.read_idx  = 0;
    media_pb_item->video_images_q.write_idx = 0;
    media_pb_item->video_images_q.size      = 0;
    memset(&media_pb_item->video_images_q.video_textures, 0, sizeof(media_pb_item->video_images_q.video_textures));

    pthread_mutex_init(&media_pb_item->video_images_q.q_mutex, NULL);
    pthread_cond_init(&media_pb_item->video_images_q.q_cond, NULL);
*/

    media_pb_item->quit = 0;
    media_pb_item->initialized = 0;
}

int media_open_for_playback(MEDIA_PLAYBACK_ITEM* media_pb_item, const char* fqpn)
{
    int i = 0;

    // Open video file
    if(avformat_open_input(&media_pb_item->format_ctx, fqpn, NULL, NULL)!=0)
    {
        fprintf(stderr, "Could not open file\n");
        media_pb_item->quit = 1;
        return -1; // Couldn't open file
    }

    // Retrieve stream information
    if(avformat_find_stream_info(media_pb_item->format_ctx, NULL) < 0)
    {
        fprintf(stderr, "Could not find stream information\n");
        media_pb_item->quit = 1;
        return -1; // Couldn't open file
    }

    // Dump information about file onto standard error
    av_dump_format(media_pb_item->format_ctx, 0, fqpn, 0);

    // We are finding a video stream here (type AVMEDIA_TYPE_VIDEO). We can
    // use inbuilt function av_find_best_stream for that.
    for(i = 0; i < media_pb_item->format_ctx->nb_streams; i++)
    {
        if(media_pb_item->format_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            media_pb_item->video_stream_idx = i;
            break;
        }
    }

    // We are finding a video stream here (type AVMEDIA_TYPE_VIDEO). We can
    // use inbuilt function av_find_best_stream for that.
    for(i = 0; i < media_pb_item->format_ctx->nb_streams; i++)
    {
        if(media_pb_item->format_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            media_pb_item->audio_stream_idx = i;
            break;
        }
    }

    // Get a pointer to the codec context for the video stream
    media_pb_item->v_codec_ctx = media_pb_item->format_ctx->streams[media_pb_item->video_stream_idx]->codec;
    
    // Find the decoder for the video stream
    media_pb_item->v_codec = avcodec_find_decoder(media_pb_item->v_codec_ctx->codec_id);
    if(media_pb_item->v_codec == NULL)
    {
        fprintf(stderr, "Unsupported Video codec!\n");
        media_pb_item->quit = 1;
        return -1; // Codec not found
    }
    
    // Open Video codec
    if(avcodec_open2(media_pb_item->v_codec_ctx, media_pb_item->v_codec, NULL) < 0)
    {
        fprintf(stderr, "Unable to open Video Codec\n");
        media_pb_item->quit = 1;
        return -1; // Could not open codec
    }

    // Get a pointer to the codec context for the video stream
    media_pb_item->a_codec_ctx = media_pb_item->format_ctx->streams[media_pb_item->audio_stream_idx]->codec;

    // Find the decoder for the audio stream
    media_pb_item->a_codec = avcodec_find_decoder(media_pb_item->a_codec_ctx->codec_id);
    if(media_pb_item->a_codec == NULL)
    {
        fprintf(stderr, "Unsupported Audio codec!\n");
        media_pb_item->quit = 1;
        return -1; // Codec not found
    }
    
    // Open audio codec
    if(avcodec_open2(media_pb_item->a_codec_ctx, media_pb_item->a_codec, NULL) < 0)
    {
        fprintf(stderr, "Unable to open Audio Codec\n");
        media_pb_item->quit = 1;
        return -1; // Could not open codec
    }

    media_pb_item->initialized = 1;
    media_pb_item->quit = 0;

    return 0;
}

#if 0
static int enqueue_video_image(MEDIA_PLAYBACK_ITEM* media_pb_item, AVFrame* frame)
{
    MEDIA_RENDER_IMAGE_Q* v_image_q = &media_pb_item->video_images_q;

    // Lock Mutex
    pthread_mutex_lock(&v_image_q->q_mutex);
    
    // Wait if Q is full
    while(v_image_q->size >= MEDIA_VIDEO_DISPLAY_FRAME_Q_SIZE && !media_pb_item->quit)
    {
        pthread_cond_wait(&v_image_q->q_cond, &v_image_q->q_mutex);
    }

    pthread_mutex_unlock(&v_image_q->q_mutex);

    MEDIA_VIDEO_TEXTURE* mvt = &v_image_q->video_textures[v_image_q->write_idx];

    if(!mvt->texture ||
       mvt->width != media_pb_item->v_codec_ctx->width ||
       mvt->height != media_pb_item->v_codec_ctx->height)
    {
        SDL_Event event;

        mvt->allocated = 0;
        event.type = GUP_VIDEO_IMAGE_ALLOC_EVENT;
        event.user.data1 = media_pb_item;
        SDL_PushEvent(&event);

        /* Wait until pict is allocated */
        pthread_mutex_lock(&v_image_q->q_mutex);
        while(!mvt->allocated && !media_pb_item->quit)
        {
            pthread_cond_wait(&v_image_q->q_cond, &v_image_q->q_mutex);
        }
        pthread_mutex_unlock(&v_image_q->q_mutex);

        if(media_pb_item->quit)
        {
            q_abort(&media_pb_item->v_packet_q);
            q_abort(&media_pb_item->a_packet_q);
            return -1;
        }
    }

    if(mvt->allocated)
    {
        int size = mvt->width * mvt->height;
        uint8_t* pixels;
        int      pitch;

        SDL_LockTexture(mvt->texture, NULL, (void **)&pixels, &pitch);
        memcpy(pixels, frame->data[0], size);
        memcpy(pixels + size, frame->data[2], size/4);
        memcpy(pixels + size*5/4, frame->data[1], size/4);
        SDL_UnlockTexture(mvt->texture);
        SDL_UpdateTexture(mvt->texture, NULL, pixels, pitch);

        if(++v_image_q->write_idx == MEDIA_VIDEO_DISPLAY_FRAME_Q_SIZE)
        {
            v_image_q->write_idx = 0;
        }

        pthread_mutex_lock(&v_image_q->q_mutex);
        v_image_q->size++;
        pthread_mutex_unlock(&v_image_q->q_mutex);
    }

    return 0;
}

/**
 *  This thread will pick up the raw decoded frames and convert them into
 *  SDL textures that will  put in a queue.
 */
void* video_thread(void* data)
{
    MEDIA_PLAYBACK_ITEM* media_pb_item = (MEDIA_PLAYBACK_ITEM*)data;

    AVFrame* source_frame       = NULL;
    AVFrame* converted_frame    = NULL;
    unsigned char* buffer       = NULL;

    /* initialize the video frame */
    source_frame = avcodec_alloc_frame();
    converted_frame = avcodec_alloc_frame();

    buffer = (uint8_t*)av_malloc(sizeof(uint8_t) * avpicture_get_size(AV_PIX_FMT_YUV420P,
                                                                      media_pb_item->v_codec_ctx->width,
                                                                      media_pb_item->v_codec_ctx->height));

    avpicture_fill((AVPicture*)converted_frame, buffer,
                   PIX_FMT_YUV420P, media_pb_item->v_codec_ctx->width,
                   media_pb_item->v_codec_ctx->height);

    // Conversion context
    struct SwsContext* sws_convert_context = NULL;

    while(!media_pb_item->initialized);

    while(1)
    {
        AVPacket packet;
        int frame_over;
        Uint32 timer_start = SDL_GetTicks();

        if(media_pb_item->quit)
        {
            q_abort(&media_pb_item->v_packet_q);
            q_abort(&media_pb_item->a_packet_q);
            break;
        }

        if(q_dequeue(&media_pb_item->v_packet_q, &packet, sizeof(AVPacket)) != Q_OK)
        {
            // Some Error or Q Over and aborted
            break;
        }

        // Decode video frame
        avcodec_decode_video2(media_pb_item->v_codec_ctx,
                              source_frame, &frame_over,
                              &packet);
            
        // Did we get a video frame?
        if(frame_over)
        {
            // Add the picture
            sws_convert_context = sws_getCachedContext(sws_convert_context,
                                                       media_pb_item->v_codec_ctx->width, media_pb_item->v_codec_ctx->height,
                                                       media_pb_item->v_codec_ctx->pix_fmt,
                                                       media_pb_item->v_codec_ctx->width, media_pb_item->v_codec_ctx->height,
                                                       AV_PIX_FMT_YUV420P,
                                                       SWS_SPLINE, NULL, NULL, NULL);

            //convert frame to YUV for Displaying
            sws_scale(sws_convert_context, source_frame->data, source_frame->linesize, 0,
                      media_pb_item->v_codec_ctx->height, converted_frame->data, converted_frame->linesize);

            if(enqueue_video_image(media_pb_item, converted_frame) < 0)
            {
                break;
            }
            //fprintf(stderr, "%s: %d ticks\n", __FUNCTION__, SDL_GetTicks() - timer_start);
        }
        av_free_packet(&packet);
    }

    av_free(buffer);
    avcodec_free_frame(&source_frame);
    avcodec_free_frame(&converted_frame);
}
#endif

const AVFrame* decode_video(MEDIA_PLAYBACK_ITEM* media_pb_item)
{
    static int      allocate           = 1;
    static AVFrame* source_frame       = NULL;
    static AVFrame* converted_frame    = NULL;
    static unsigned char* buffer       = NULL;

    static struct SwsContext* sws_convert_context = NULL;

    if(allocate)
    {
        /* initialize the video frame when we enter first*/
        source_frame    = avcodec_alloc_frame();
        converted_frame = avcodec_alloc_frame();

        buffer = (uint8_t*)av_malloc(sizeof(uint8_t) * avpicture_get_size(AV_PIX_FMT_YUV420P,
                                                                          media_pb_item->v_codec_ctx->width,
                                                                          media_pb_item->v_codec_ctx->height));

        avpicture_fill((AVPicture*)converted_frame, buffer,
                       PIX_FMT_YUV420P, media_pb_item->v_codec_ctx->width,
                       media_pb_item->v_codec_ctx->height);

        /* set allocate to 0 so that future calls reuse the frames */
        allocate = 0;
    }

    if(media_pb_item->quit)
    {
        avcodec_free_frame(&source_frame);
        avcodec_free_frame(&converted_frame);
        av_free(buffer);
        return NULL;
    }

    for(;;)
    {
        AVPacket packet;
        int frame_over;

        if(q_dequeue(&media_pb_item->v_packet_q, &packet, sizeof(AVPacket)) != Q_OK)
        {
            // Some Error or Q Over and aborted
            break;
        }

        avcodec_decode_video2(media_pb_item->v_codec_ctx,
                              source_frame, &frame_over,
                              &packet);
            
        // Did we get a video frame?
        if(frame_over)
        {
            // Add the picture
            sws_convert_context = sws_getCachedContext(sws_convert_context,
                                                       media_pb_item->v_codec_ctx->width, media_pb_item->v_codec_ctx->height,
                                                       media_pb_item->v_codec_ctx->pix_fmt,
                                                       media_pb_item->v_codec_ctx->width, media_pb_item->v_codec_ctx->height,
                                                       AV_PIX_FMT_YUV420P,
                                                       SWS_SPLINE, NULL, NULL, NULL);

            //convert frame to YUV for Displaying
            sws_scale(sws_convert_context, (const uint8_t* const*)source_frame->data, source_frame->linesize, 0,
                      media_pb_item->v_codec_ctx->height, converted_frame->data, converted_frame->linesize);

        
            av_free_packet(&packet);

            return converted_frame;
        }

        av_free_packet(&packet);
    }

    avcodec_free_frame(&source_frame);
    avcodec_free_frame(&converted_frame);
    av_free(buffer);

    return NULL;
}

/**
 *  This thread is an infinite loop, getting media packets and putting them
 *  onto a relevant Q for further processing
 */
void*  media_read_thread(void* data)
{
    MEDIA_PLAYBACK_ITEM* media_pb_item = (MEDIA_PLAYBACK_ITEM*)data;
    AVPacket    packet;

    /* initialize packet, set data to NULL, let the demuxer fill it */
    av_init_packet(&packet);
    packet.data = NULL;
    packet.size = 0;

    while(!media_pb_item->initialized);

    while(1)
    {
        Uint32 timer_start = SDL_GetTicks();
        if(media_pb_item->quit)
        {
            q_abort(&media_pb_item->v_packet_q);
            q_abort(&media_pb_item->a_packet_q);
            break;
        }

        if(av_read_frame(media_pb_item->format_ctx, &packet) < 0)
        {
            if(media_pb_item->format_ctx->pb == 0)
            {
                /* No Error. Wait for user input */
                //sleep(1);
                //SDL_Delay(100);
                continue;
            }
            else
            {
                break;
            }
        }

        // Is this a packet from the video stream?
        if(packet.stream_index == media_pb_item->video_stream_idx)
        {
            int q_status = q_enqueue(&media_pb_item->v_packet_q, &packet, sizeof(AVPacket));

            if(q_status != Q_OK)
            {
                fprintf(stderr, "Raw Decoded Frame Enqueue Error: %d\n", q_status);
                break;
            }
            //fprintf(stderr, "%s: %d ticks\n", __FUNCTION__, SDL_GetTicks() - timer_start);
        }
        else if(packet.stream_index == media_pb_item->audio_stream_idx)
        {
            int q_status = q_enqueue(&media_pb_item->a_packet_q, &packet, sizeof(AVPacket));

            if(q_status != Q_OK)
            {
                fprintf(stderr, "Raw Decoded Frame Enqueue Error: %d\n", q_status);
                break;
            }
        }
        else
        {
            // Free the packet that was allocated by av_read_frame
            av_free_packet(&packet);
        }
    }

    // Write Qignal QUIT Code here
}

uint8_t** decode_audio(MEDIA_PLAYBACK_ITEM* media_pb_item,
                       uint8_t*             audio_buffer,
                       int*                 audio_dst_bufsize)
{
    /* static int          allocate    = 1; */
    static AVFrame      frame;
    static AVPacket     packet;
    /* static uint8_t**    audio_dst_data = NULL; */

    static struct SwrContext *swr_ctx = NULL;

    /* if(allocate) */
    /* { */
    /*     /\* initialize the video frame when we enter first*\/ */
    /*     frame    = avcodec_alloc_frame(); */

    /*     /\* set allocate to 0 so that future calls reuse the frames *\/ */
    /*     allocate = 0; */

    /*     av_init_packet(&packet); */
    /*     packet.data = NULL; */
    /*     packet.size = 0; */

    /*     /\* int nb_planes = av_sample_fmt_is_planar(media_pb_item->a_codec_ctx->sample_fmt) ?  *\/ */
    /*     /\*     media_pb_item->a_codec_ctx->channels : 1; *\/ */
    /*     /\* audio_dst_data = av_mallocz(sizeof(uint8_t *) * nb_planes); *\/ */
    /* } */

    if(media_pb_item->quit)
    {
        /* avcodec_free_frame(&frame); */
        /* av_free_packet(&packet); */
        return NULL;
    }

    for(;;)
    {
        int frame_over;
        int decoded;

        while(packet.size > 0)
        {
            int ret = avcodec_decode_audio4(media_pb_item->a_codec_ctx, &frame, &frame_over, &packet);
            if (ret < 0)
            {
                fprintf(stderr, "Error Decoding Audio Frame\n");
                return NULL;
            }

            decoded = FFMIN(ret, packet.size);

            packet.data += decoded;
            packet.size -= decoded;
                
            if(frame_over)
            {
                /* if(frame->nb_samples > *previous_samples) */
                /* { */
                /*     if(0 != *previous_samples) */
                /*     { */
                /*         /\* Not the first time *\/ */
                /*         av_freep(&audio_dst_data[0]); */
                /*     } */
                    
                /*     ret = av_samples_alloc(audio_dst_data, audio_dst_linesize, av_frame_get_channels(frame), */
                /*                            frame->nb_samples, frame->format, 1); */
                /* } */
                
                *audio_dst_bufsize = av_samples_get_buffer_size(NULL, media_pb_item->a_codec_ctx->channels,
                                                                frame.nb_samples, media_pb_item->a_codec_ctx->sample_fmt, 1);

                /* copy audio data to destination buffer:
                 * this is required since rawaudio expects non aligned data */
                /* av_samples_copy(audio_dst_data, frame->data, 0, 0, */
                /*                 frame->nb_samples, av_frame_get_channels(frame), frame->format); */

                /* *previous_samples = frame->nb_samples; */

                memcpy(audio_buffer, frame.data[0], *audio_dst_bufsize);
                return NULL;
                /* return audio_dst_data; */
            }
        }

        /* av_free_packet(&packet); */
        /* av_init_packet(&packet); */
        /* packet.data = NULL; */
        /* packet.size = 0; */

        /* dequeue the next packet */
        if(q_dequeue(&media_pb_item->a_packet_q, &packet, sizeof(AVPacket)) != Q_OK)
        {
            // Some Error or Q Over and aborted
            break;
        }
    }

    /* avcodec_free_frame(&frame); */
    /* av_free_packet(&packet); */
    /* av_freep(&audio_dst_data[0]); */
    /* av_freep(audio_dst_data); */

    return NULL;
}
