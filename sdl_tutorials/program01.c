#include <SDL2/sdl.h>
//#include <conio.h>

int framenumber = 0;
int delay = 1000;

Uint32 fps_regulator(Uint32 interval, void* f)
{
    printf("avg_fps = %d\n", framenumber);
    framenumber = 0;
    return interval;
}

SDL_Texture* get_bmp_texture(int filenumber, SDL_Renderer* renderer)
{
    SDL_Surface* image;
    SDL_Texture* image_textured;
    
    char filename[4096] = {0};
    sprintf(filename, "/Users/amadhusudan/development/tutorial01/frame%d.bmp", filenumber);
    
    image = SDL_LoadBMP(filename);
    image_textured = SDL_CreateTextureFromSurface(renderer, image);
    SDL_FreeSurface(image);
    
    return image_textured;
}
int main(int argc, char** argv)
{
    //The images    
    SDL_Window*     window      = NULL;
    SDL_Renderer*   renderer    = NULL;
    SDL_Surface*    hello       = NULL;
    SDL_Texture*    helloTex    = NULL;
    int posX = 100, posY = 100, width = 320, height = 240;

    
    // STart SDL
    SDL_Init(SDL_INIT_EVERYTHING);
    
    window = SDL_CreateWindow("SDL_RenderClear",
                              SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              1024, 768,
                              SDL_WINDOW_SHOWN);
    // We must call SDL_CreateRenderer in order for draw calls to affect this window.
    renderer = SDL_CreateRenderer(window, -1, 0);
    
    // Select the color for drawing. It is set to red here.
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    
    // Clear the entire screen to our selected color.
    SDL_RenderClear(renderer);
    
    //Load image
    hello = SDL_LoadBMP( "hello.bmp" );
    helloTex = SDL_CreateTextureFromSurface(renderer, hello);
    SDL_FreeSurface(hello);

    int filenumber = 1;
    Uint32 fps = 20;
    Uint32 timer_start = SDL_GetTicks();
    
    SDL_TimerID fps_timer_id = SDL_AddTimer(delay, fps_regulator, NULL);

    while (1) {
        SDL_Event e;
        if (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                break;
            }
        }
        
        SDL_RenderClear(renderer);
        SDL_Texture* to_render = get_bmp_texture(filenumber, renderer);
        SDL_RenderCopy(renderer, to_render, NULL, NULL);
        SDL_RenderPresent(renderer);
        SDL_DestroyTexture(to_render);

        framenumber++;filenumber++;
        if(filenumber>100) filenumber = 0;
        
        if ((1000 / fps) > SDL_GetTicks() - timer_start)
        {
            SDL_Delay((1000 / fps) - (SDL_GetTicks() - timer_start));
        }
}
    
    // Up until now everything was drawn behind the scenes.
    // This will show the new, red contents of the window.
    //SDL_RenderPresent(renderer);

    
    // Give us time to see the window.
    //SDL_Delay(5000);

    SDL_DestroyTexture(helloTex);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    //Quit SDL
    SDL_Quit();
    
    return 0;
}